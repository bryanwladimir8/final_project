<h1>Listado de Detalles</h1>
<br>
<br>
<div class="row">
    <div class="col-md-12">
        <?php if ($detalle): ?>
            <table class="table  table-striped" id="tablaDetalle">
                <thead>
                    <th>ID</th>
                    <th>Lectura</th>
                    <th>Recaudacion</th>
                    <th>Cantida Detalle </th>
                    <th>Detalle</th>
                    <th>Valor Unitario</th>
                    <th>Subtotal</th>
                    <th>Acciones</th>
                </thead>
                
                <tbody>
                    <?php foreach ($detalle as $filaTemporal): ?>
                        <tr>
                            <td><?php echo $filaTemporal->id_det ?></td>
                            <td><?php echo $filaTemporal->fk_id_lec?></td>
                            <td><?php echo $filaTemporal->fk_id_rec ?>  </td>
                            <td> <?php echo $filaTemporal->cantidad_det ?></td>
                            <th><?php echo $filaTemporal->detalle_det ?></th>
                            <th><?php echo $filaTemporal->valor_unitario_det ?></th>
                            <th><?php echo $filaTemporal->subtotal_det ?></th>
                            <td class="text-center" >
                                <a href="<?php echo site_url(); ?>/detalles/editar/<?php echo $filaTemporal->id_det; ?>" title="Editar Detalle" >
                                    <i class="mdi  mdi-pencil">Editar</i>
                                </a>
                                &nbsp;&nbsp;

                                <a href="<?php echo site_url(); ?>/detalles/eliminar/<?php echo $filaTemporal->id_det; ?>" title="Borrar Detalle" style="color:red" onclick="return confirm('Esta seguro de borra el registro?');">
                                    <i class="mdi  mdi-close">Eliminar</i>
                                </a>
            
                            </td>
                        </tr>         

                    <?php endforeach; ?>
                    </tbody>
                </table>

                <?php else: ?>
                <h1>No hay datos</h1>

        <?php endif; ?>

       
    </div>
</div>

<script type="text/javascript">
    $("#tablaDetalle")
    .DataTable();
</script>