<h1>Listado de  Usuarios</h1>
<br>
<br>
<div class="row">
    <div class="col-md-12">
        <?php if ($usuarios): ?>
            <table class="table  table-striped" id="tablaUsuarios">
                <thead>
                    <th>ID</th>
                    <th>Nombre Usuario</th>
                    <th>Apellido</th>
                    <th>Correo Electrónico</th>
                    <th>Estado</th>
                    <th>Acciones</th>
                </thead>
                
                <tbody>
                    <?php foreach ($usuarios as $filaTemporal): ?>
                        <tr>
                            <td><?php echo $filaTemporal->id_usu ?></td>
                            <td><?php echo $filaTemporal->nombre_usu ?></td>
                            <td><?php echo $filaTemporal->apellido_usu ?></td>
                            <td> <?php echo $filaTemporal->email_usu ?></td>
                            <td><?php echo $filaTemporal->estado_usu ?></td>
                            
                            <td class="text-center" >
                                <a href="<?php echo site_url(); ?>/usuarios/editar/<?php echo $filaTemporal->id_usu; ?>" title="Editar Instructor" >
                                    <i class="mdi  mdi-pencil">Editar</i>
                                </a>
                                &nbsp;&nbsp;

                                <a href="<?php echo site_url(); ?>/usuarios/eliminar/<?php echo $filaTemporal->id_usu; ?>" title="Borrar Instructor" style="color:red" onclick="return confirm('Esta seguro de borra el registro?');">
                                    <i class="mdi  mdi-close">Eliminar</i>
                                </a>
            
                            </td>
                        </tr>         

                    <?php endforeach; ?>
                    </tbody>
                </table>

                <?php else: ?>
                <h1>No hay datos</h1>

        <?php endif; ?>

       
    </div>
</div>

<script type="text/javascript">
    $("#tablaUsuarios")
    .DataTable();
</script>