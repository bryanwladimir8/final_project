<h1>Nuevo Usuario</h1>
<div class="row">
    <div class="col-md-12">
        <form action="<?php echo site_url(); ?>/usuarios/guardar" method="post">

            <div class="row">
                <div class="col-md-6">

                <label for="">Nombre:</label>
                <br>
                <input type="text" class="form-control" name="nombre_usu" value="" id="nombre_usu" placeholder="Ingrese su nombre">

                </div>
                <div class="col-md-6">

                <label for=""> Apellido:</label>
                <br>
                <input type="text" class="form-control"name="apellido_usu" value="" id="apellido_usu" placeholder="Ingrese su Apellido">

                </div>
               

            </div>
            <div class="row">
                <div class="col-md-6">
                    <label for="">Email:</label>
                    <br>
                    <input type="text" class="form-control"name="email_usu" id="email_usu" value=""  placeholder="Ingrese el correo">

                </div>
                <div class="col-md-6">
                    <label for="">Estado:</label>
                    <br>
                    <select name="estado_usu" id="estado_usu" class="form-control">
                        <option value="ACTIVO">Activo</option>
                        <option value="INACTIVO">Inactivo</option>
                    </select>

                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <button type="submit" name="button" class="btn btn-primary">Guardar</button>&nbsp;
                    <a href="<?php echo site_url(); ?>/usuarios/guardar" class="btn btn-danger">Cancelar </a>
                </div>
            </div>   
        </form>
    </div>
</div>