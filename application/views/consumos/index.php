<div class="row">
    <div class="col-md-6">
        <h1>Listado de Consumos</h1>
    </div>
    <div>
        <a href="<?php echo site_url();?>/consumos/nuevo">Agregar Nuevo </a>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-12">
        <?php if ($consumo): ?>
            <table class="table  table-striped" id="tablaSocios">
                <thead>
                    <th>ID</th>
                    <th>Año consumo</th>
                    <th>Mes consumo</th>
                    <th>Estado </th>
                    <th>Fecha de Creacion</th>
                    <th>Fecha de Actualizacion</th>
                    <th>Numero Mes Consumo</th>
                    <th>Fecha Vencimiento</th>
                    <th>Acciones</th>
                </thead>
                
                <tbody>
                    <?php foreach ($consumo as $filaTemporal): ?>
                        <tr>
                            <td><?php echo $filaTemporal->id_consumo ?></td>
                            <td><?php echo $filaTemporal->anio_consumo ?></td>
                            <td><?php echo $filaTemporal->mes_consumo ?>  </td>
                            <td><?php echo $filaTemporal->estado_consumo ?></td>
                            <td><?php echo $filaTemporal->fecha_creacion_consumo ?></td>
                            <th><?php echo $filaTemporal->fecha_actualizacion_consumo ?></th>
                            <th><?php echo $filaTemporal->numero_mes_consumo ?></th>
                            <th><?php echo $filaTemporal->fecha_vencimiento_consumo ?></th>

                            <td class="text-center" >
                                <a href="<?php echo site_url(); ?>/consumos/editar/<?php echo $filaTemporal->id_consumo; ?>" title="Editar Consumo" >
                                    <i class="mdi  mdi-pencil">Editar</i>
                                </a>
                                &nbsp;&nbsp;

                                <a href="<?php echo site_url(); ?>/consumos/eliminar/<?php echo $filaTemporal->id_consumo; ?>" title="Borrar Consumo" style="color:red" onclick="return confirm('Esta seguro de borra el registro?');">
                                    <i class="mdi  mdi-close">Eliminar</i>
                                </a>
            
                            </td>
                        </tr>         

                    <?php endforeach; ?>
                    </tbody>
                </table>

                <?php else: ?>
                <h1>No hay datos</h1>

        <?php endif; ?>

       
    </div>
</div>

<script type="text/javascript">
    $("#tablaSocios")
    .DataTable();
</script>