<div class="row">
    <div class="col-md-6">
        <h1>Listado de Impuestos</h1>
    </div>
    <div>
        <a href="<?php echo site_url();?>/impuestos/nuevo">Agregar Nuevo </a>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-12">
        <?php if ($impuesto): ?>
            <table class="table  table-striped" id="tablaImpuesto">
                <thead>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Descipcion</th>
                    <th>Porcentaje</th>
                    <th>Retencion</th>
                    <th>Estado</th>
                    <th>Creacion</th>
                    <th>Actualizacion</th>
                    <th>Acciones</th>
                </thead>

                <tbody>
                    <?php foreach ($impuesto as $filaTemporal): ?>
                        <tr>
                            <td><?php echo $filaTemporal->id_imp ?></td>
                            <td><?php echo $filaTemporal->nombre_imp ?></td>
                            <td><?php echo $filaTemporal->descripcion_imp ?>  </td>
                            <td><?php echo $filaTemporal->porcentaje_imp ?></td>
                            <th><?php echo $filaTemporal->retencion_imp ?></th>
                            <th><?php echo $filaTemporal->estado_imp ?></th>
                            <th><?php echo $filaTemporal->creacion_imp ?></th>
                            <th><?php echo $filaTemporal->actualizacion_imp ?></th>


                            <td class="text-center" >
                                <a href="<?php echo site_url(); ?>/impuestos/editar/<?php echo $filaTemporal->id_imp; ?>" title="Editar impuestos" >
                                    <i class="mdi  mdi-pencil">Editar</i>
                                </a>
                                &nbsp;&nbsp;

                                <a href="<?php echo site_url(); ?>/impuestos/eliminar/<?php echo $filaTemporal->id_imp; ?>" title="Borrar impuestos" style="color:red" onclick="return confirm('Esta seguro de borra el registro?');">
                                    <i class="mdi  mdi-close">Eliminar</i>
                                </a>

                            </td>
                        </tr>

                    <?php endforeach; ?>
                    </tbody>
                </table>

                <?php else: ?>
                <h1>No hay datos</h1>

        <?php endif; ?>


    </div>
</div>

<script type="text/javascript">
    $("#tablaImpuesto")
    .DataTable();
</script>
