<div class="row">
    <div class="col-md-6">
        <h1>Nuevo Impuesto</h1>
    </div>
    <div>
        <a href="<?php echo site_url();?>/impuestos/nuevo">Agregar Nuevo </a>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <form action="<?php echo site_url(); ?>/impuestos/guardar" method="post">


                <div class="row">
                    <div  class="col-md-4">
                        <label for=""> Nombre:</label>
                        <br>
                        <input type="text" class="form-control"name="nombre_imp" value="" id="descripcion_imp" placeholder="Ingrese el nombre">

                    </div>
                    <div class="col-md-4">
                        <label for="">Descripcion:</label>
                        <br>
                        <input type="text" class="form-control"name="descripcion_imp" value="" id="descripcion_imp" placeholder="Ingrese la descripcion">


                        </div>
                    <div class="col-md-4">
                        <label for=""> Porcentaje:</label>
                        <br>
                        <input type="text" class="form-control"name="porcentaje_imp" value="" id="porcentaje_imp" placeholder="Ingrese el Porcentaje">
                    </div>

                </div>


              </div>
              <div class="row">
                  <div  class="col-md-4">
                      <label for=""> Retencion:</label>
                      <br>
                      <input type="text" class="form-control" name="retencion_imp" value="" id="retencion_imp" placeholder="Ingrese la Retencion">

                  </div>

                  <div  class="col-md-4">
                      <label for=""> Estado:</label>
                      <br>
                      <input type="text" class="form-control" name="estado_imp" value="" id="estado_imp" placeholder="Ingrese el Estado">

                  </div>

                  <div  class="col-md-4">
                      <label for=""> Creacion:</label>
                      <br>
                      <input type="date" class="form-control" name="creacion_imp" value="" id="creacion_imp" placeholder="Ingrese la fecha de creacion">

                  </div>
                  <div  class="col-md-4">
                      <label for=""> Actualizacion:</label>
                      <br>
                      <input type="date" class="form-control" name="actualizacion_imp" value="" id="actualizacion_imp" placeholder="Ingrese la fecha de actualizacion">

                  </div>

              </div>

            </div>
            <br>
            <br>

            <div class="row">
                <div class="col-md-12 text-center">
                    <button type="submit" name="button" class="btn btn-primary">Guardar</button>&nbsp;
                    <a href="<?php echo site_url(); ?>/impuestos/index" class="btn btn-danger">Cancelar </a>
                </div>
            </div>
        </form>
    </div>
</div>
