<div class="row">
    <div class="col-md-6">
        <h1>Nuevo Impuesto</h1>
    </div>
    <div>
        <a href="<?php echo site_url();?>/impuestos/procesoActualizar">Agregar Nuevo</a>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <form action="<?php echo site_url(); ?>/impuestos/guardar" method="post">
            <input type="text" name="id_imp" id="id_imp" value="<?php echo $impuestoEditar->id_imp ?>" hidden/>
            <div class="row">
                <div  class="col-md-4">
                    <label for=""> Nombre:</label>
                    <  <br>
                      <input type="text" class="form-control"name="nombre_imp" value="<?php echo $impuestoEditar->nombre_imp ?>" id="nombre_imp" placeholder="Ingrese el nombre">

            </div>
            <div class="row">
                <div  class="col-md-6">
                    <label for=""> Descripcion:</label>
                    <br>
                    <input type="text" class="form-control" name="descripcion_imp" value="<?php echo $impuestoEditar->descripcion_imp ?>" id="descripcion_imp" placeholder="Ingrese la Descripcion">

                </div>

                <div  class="col-md-6">
                    <label for=""> Porcentaje:</label>
                    <br>
                    <input type="text" class="form-control" name="porcentaje_imp" value="<?php echo $impuestoEditar->porcentaje_imp ?>" id="porcentaje_imp" placeholder="Ingrese el porcentaje">

                </div>
                <div  class="col-md-6">
                    <label for=""> Retencion:</label>
                    <br>
                    <input type="text" class="form-control" name="retencion_imp" value="<?php echo $impuestoEditar->retencion_imp ?>" id="retencion_imp" placeholder="Ingrese la retencion">

                </div>
                <div  class="col-md-6">
                    <label for=""> Estado:</label>
                    <br>
                    <input type="text" class="form-control" name="estado_imp" value="<?php echo $impuestoEditar->estado_imp ?>" id="estado_imp" placeholder="Ingrese el estado">

                </div>

                <div  class="col-md-6">
                    <label for=""> Creacion:</label>
                    <br>
                    <input type="date" class="form-control" name="creacion_imp" value="<?php echo $impuestoEditar->creacion_imp ?>" id="creacion_imp" placeholder="Ingrese la fecha de creacion">

                </div>
                <div  class="col-md-6">
                    <label for=""> Actualizacion:</label>
                    <br>
                    <input type="date" class="form-control" name="actualizacion_imp" value="<?php echo $impuestoEditar->actualizacion_imp ?>" id="actualizacion_imp" placeholder="Ingrese la fecha de actualizacion">

                </div>

            </div>
            <br>
            <br>

            <div class="row">
                <div class="col-md-12 text-center">
                    <button type="submit" name="button" class="btn btn-primary">Guardar</button>&nbsp;
                    <a href="<?php echo site_url(); ?>/impuestos/index" class="btn btn-danger">Cancelar </a>
                </div>
            </div>
        </form>
    </div>
</div>
