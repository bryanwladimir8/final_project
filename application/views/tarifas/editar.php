<div class="container-fluid">
    <div class="card">
        <center>
            <h1>
                <b>
                    FORMULARIO EDITAR NUEVA TARIFA
                </b>
            </h1>
        </center>
        <div class="card-body">
            <form action="<?php echo site_url('/Tarifas/EditarTarifa') ?>" method="post" id="frm_nuevo_tarifa">
            <div class="col-4">
                        <div class="mb-3">
                            <input type="hidden"  value="<?php echo $tarifaEditar->id_tar ?>" type="text" class="form-control" name="id_tar" id="id_tar" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                <div class="row justify-content-center align-items-center g-2">
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="nombre_tar" class="form-label">NOMBRE:</label>
                            <input  value="<?php echo $tarifaEditar->nombre_tar ?>" type="text" class="form-control" name="nombre_tar" id="nombre_tar" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="descripcion_tar" class="form-label">DESCRIPCION:</label>
                            <input value="<?php echo $tarifaEditar->descripcion_tar ?>" type="text" class="form-control" name="descripcion_tar" id="descripcion_tar" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="estado_tar" class="form-label">ESTADO:</label>
                            <input value="<?php echo $tarifaEditar->estado_tar ?>" type="text" class="form-control" name="estado_tar" id="estado_tar" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="m3_maximo_tar" class="form-label">TARIFA MAXIMA:</label>
                            <input value="<?php echo number_format($tarifaEditar->m3_maximo_tar, 2, '.', ''); ?>" type="text" class="form-control" name="m3_maximo_tar" id="m3_maximo_tar" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="tarifa_basica_tar" class="form-label">TARIFA BASICA:</label>
                            <input value="<?php echo number_format($tarifaEditar->tarifa_basica_tar, 2, '.', ''); ?>" type="text" class="form-control" name="tarifa_basica_tar" id="tarifa_basica_tar" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="tarifa_excedente_tar" class="form-label">TARIFA EXCEDENTE:</label>
                            <input value="<?php echo number_format($tarifaEditar->tarifa_excedente_tar, 2, '.', ''); ?>" type="text" class="form-control" name="tarifa_excedente_tar" id="tarifa_excedente_tar" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="valor_mora_tar" class="form-label">TARIFA MORA:</label>
                            <input value="<?php echo number_format($tarifaEditar->valor_mora_tar, 2, '.', ''); ?>" type="text" class="form-control" name="valor_mora_tar" id="valor_mora_tar" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                   
                    <center>
                        <button  class="btn btn-primary" type="submit">Guardar</button>
                        <a name="" id="" class="btn btn-dark" href="<?php echo site_url('/Tarifas/index') ?>" role="button">Cancelar</a>
                    </center>
            </form>
        </div>
    </div>
</div>
</div>
</div>
<script type="text/javascript">
    $("#frm_nuevo_tarifa").validate({
        rules: {
            nombre_tar: {
                required: true,
                minlength: 10,
                maxlength: 10,
                digits: true
            },
            descripcion_tar: {
                required: true,

                letras: true
            },
            estado_tar: {
                required: true,

                letras: true
            },
            


        },
        messages: {
            nombre_tar: {
                required: "Nesesariamente debe elegir una",
                minlength: "Cedula incorrecta",
                maxlength: "Cedula incorrecta 10 digitos",
                digits: "Este campo solo acepta numeros",
                number: "Este campo solo acepta numeros",
            },
            descripcion_tar: {
                required: "Porfavor ingrese la descripcion del delito",
                // minlength: "El apellido debe tener minimo 3 digitos ",
                // maxlength: "APellido incorrecto"
            },
            pestado_tar: {
                required: "debe ingresar el tiempo de condena para el delito",
                // minlength: "El nombre debe tener 3 dígitos",
                // maxlength: "Nombres incorrectos 50 dígitos",
            },
            


        }
    });
</script>

</div>