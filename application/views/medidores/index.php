<div class="row">
    <div class="col-md-6">
        <h1>Listado de Medidores</h1>
    </div>
    <div>
        <a href="<?php echo site_url();?>/medidores/nuevo">Agregar Nuevo </a>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-12">
        <?php if ($medidor): ?>
            <table class="table  table-striped" id="tablaMedidores">
                <thead>
                    <th>ID Medidor</th>
                    <th>ID Ruta</th>
                    <th>ID Tarifa</th>
                    <th>Mumero Medidor</th>
                    <th>Seire Medidor</th>
                    <th>Marca Medidor</th>
                    <th>Ovservaciom Medidor</th>
                    <th>Estado Medidor</th>
                    <th>Foto Medidor</th>
                    <th>Creaciom Medidor</th>
                    <th>Actualizaciom Medidor</th>
                    <th>Lectura Imicial Medidor</th>
                    <th>Acciones</th>
                </thead>

                <tbody>
                    <?php foreach ($medidor as $filaTemporal): ?>
                        <tr>
                            <td><?php echo $filaTemporal->id_med ?></td>
                            <td><?php echo $filaTemporal->fk_id_rut ?></td>
                            <td><?php echo $filaTemporal->fk_id_tar ?>  </td>
                            <td><?php echo $filaTemporal->numero_med ?></td>
                            <td><?php echo $filaTemporal->serie_med ?></td>
                            <th><?php echo $filaTemporal->marca_med ?></th>
                            <th><?php echo $filaTemporal->observacion_med ?></th>
                            <th><?php echo $filaTemporal->estado_med ?></th>
                            <th><?php echo $filaTemporal->foto_med ?></th>
                            <th><?php echo $filaTemporal->creacion_med ?></th>
                            <th><?php echo $filaTemporal->actualizacion_med ?></th>
                            <th><?php echo $filaTemporal->lectura_inicial_med ?></th>

                            <td class="text-center" >
                                <a href="<?php echo site_url(); ?>/medidores/editar/<?php echo $filaTemporal->id_med; ?>" title="Editar Medidor" >
                                    <i class="mdi  mdi-pencil">Editar</i>
                                </a>
                                &nbsp;&nbsp;

                                <a href="<?php echo site_url(); ?>/medidores/eliminar/<?php echo $filaTemporal->id_med; ?>" title="Borrar Medidor" style="color:red" onclick="return confirm('Esta seguro de borra el registro?');">
                                    <i class="mdi  mdi-close">Eliminar</i>
                                </a>

                            </td>
                        </tr>

                    <?php endforeach; ?>
                    </tbody>
                </table>

                <?php else: ?>
                <h1>No hay datos</h1>

        <?php endif; ?>


    </div>
</div>

<script type="text/javascript">
    $("#tablaMedidores")
    .DataTable();
</script>
