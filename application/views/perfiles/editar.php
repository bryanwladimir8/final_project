<div class="container-fluid">
    <div class="card">
        <center>
            <h1>
                <b>
                    FORMULARIO EDITAR PERFILES
                </b>
            </h1>
        </center>
        <div class="card-body">
            <form action="<?php echo site_url('/Perfiles/EditarPerfil') ?>" method="post" id="frm_nuevo_perfil">
            <div class="col-4">
                        <div class="mb-3">
                            <input type="hidden"  value="<?php echo $perfilEditar->id_per ?>" type="text" class="form-control" name="id_per" id="id_per" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                <div class="row justify-content-center align-items-center g-2">
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="nombre_per" class="form-label">NOMBRE:</label>
                            <input  value="<?php echo $perfilEditar->nombre_per ?>" type="text" class="form-control" name="nombre_per" id="nombre_per" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="descripcion_per" class="form-label">DESCRIPCION:</label>
                            <input value="<?php echo $perfilEditar->descripcion_per ?>" type="text" class="form-control" name="descripcion_per" id="descripcion_per" aria-describedby="helpId" placeholder="">
                        </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="estado_per" class="form-label">ESTADO:</label>
                            <input value="<?php echo $perfilEditar->estado_per ?>" type="text" class="form-control" name="estado_per" id="estado_per" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="creacion_per" class="form-label">CREACION:</label>
                            <input value="<?php echo $perfilEditar->creacion_per ?>" type="datetime-local" class="form-control" name="creacion_per" id="creacion_per" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="actualizacion_per" class="form-label">ACTUALIZACION:</label>
                            <input value="<?php echo $perfilEditar->actualizacion_per ?>" type="datetime-local" class="form-control" name="actualizacion_per" id="actualizacion_per" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    <center>
                        <button  class="btn btn-primary" type="submit">Guardar</button>
                        <a name="" id="" class="btn btn-dark" href="<?php echo site_url('/Perfiles/index') ?>" role="button">Cancelar</a>
                    </center>
            </form>
        </div>
    </div>
</div>
</div>
</div>

<script type="text/javascript">
    $("#frm_nuevo_perfil").validate({
        rules: {
            nombre_per: {
                required: true,
                minlength: 10,
                maxlength: 10,
                digits: true
            },
            descripcion_per: {
                required: true,

                letras: true
            },
            estado_per: {
                required: true,

                letras: true
            },
            


        },
        messages: {
            nombre_per: {
                required: "Nesesariamente debe elegir una",
                minlength: "Cedula incorrecta",
                maxlength: "Cedula incorrecta 10 digitos",
                digits: "Este campo solo acepta numeros",
                number: "Este campo solo acepta numeros",
            },
            descripcion_per: {
                required: "Porfavor ingrese la descripcion del delito",
                // minlength: "El apellido debe tener minimo 3 digitos ",
                // maxlength: "APellido incorrecto"
            },
            estado_per: {
                required: "debe ingresar el tiempo de condena para el delito",
                // minlength: "El nombre debe tener 3 dígitos",
                // maxlength: "Nombres incorrectos 50 dígitos",
            },
            


        }
    });
</script>

</div>