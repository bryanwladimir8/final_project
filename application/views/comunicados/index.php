<div class="row">
    <div class="col-md-6">
        <h1>Listado de Comunicados</h1>
    </div>
    <div>
        <a href="<?php echo site_url();?>/comunicados/nuevo">Agregar Nuevo </a>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-12">
        <?php if ($comunicado): ?>
            <table class="table  table-striped" id="tablaComunicado">
                <thead>
                    <th>ID</th>
                    <th>Fecha</th>
                    <th>Mensaje</th>
                    <th>Actualizacion</th>
                    <th>Creacion</th>
                    <th>Acciones</th>
                </thead>

                <tbody>
                    <?php foreach ($comunicado as $filaTemporal): ?>
                        <tr>
                            <td><?php echo $filaTemporal->id_com ?></td>
                            <td><?php echo $filaTemporal->fecha_com ?></td>
                            <td><?php echo $filaTemporal->mensaje_com ?>  </td>
                            <td><?php echo $filaTemporal->actualizacion_com ?></td>
                            <th><?php echo $filaTemporal->creacion_com ?></th>


                            <td class="text-center" >
                                <a href="<?php echo site_url(); ?>/camunicados/editar/<?php echo $filaTemporal->id_com; ?>" title="Editar comunicados" >
                                    <i class="mdi  mdi-pencil">Editar</i>
                                </a>
                                &nbsp;&nbsp;

                                <a href="<?php echo site_url(); ?>/camunicados/eliminar/<?php echo $filaTemporal->id_com; ?>" title="Borrar comunicados" style="color:red" onclick="return confirm('Esta seguro de borra el registro?');">
                                    <i class="mdi  mdi-close">Eliminar</i>
                                </a>

                            </td>
                        </tr>

                    <?php endforeach; ?>
                    </tbody>
                </table>

                <?php else: ?>
                <h1>No hay datos</h1>

        <?php endif; ?>


    </div>
</div>

<script type="text/javascript">
    $("#tablaComunicado")
    .DataTable();
</script>
