<div class="row">
    <div class="col-md-6">
        <h1>Nuevo Comunicado</h1>
    </div>
    <div>
        <a href="<?php echo site_url();?>/comunicados/procesoActualizar">Agregar Nuevo</a>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <form action="<?php echo site_url(); ?>/comunicados/guardar" method="post">
            <input type="text" name="id_com" id="id_com" value="<?php echo $comunicadoEditar->id_com ?>" hidden/>
            <div class="row">
                <div  class="col-md-4">
                    <label for=""> fecha:</label>
                    <  <br>
                      <input type="date" class="form-control"name="nombre_imp" value="<?php echo $comunicadoEditar->fecha_com ?>" id="nombre_imp" placeholder="Ingrese la fecha">

            </div>
            <div class="row">
                <div  class="col-md-6">
                    <label for=""> Mensaje:</label>
                    <br>
                    <input type="text" class="form-control" name="descripcion_imp" value="<?php echo $comunicadoEditar->mensaje_com ?>" id="descripcion_imp" placeholder="Ingrese el porcentaje">

                </div>

                <div  class="col-md-6">
                    <label for=""> Actualizacion:</label>
                    <br>
                    <input type="date" class="form-control" name="porcentaje_imp" value="<?php echo $comunicadoEditar->actualizacion_com ?>" id="porcentaje_imp" placeholder="Ingrese la fecha de actualizacion">

                </div>
                <div  class="col-md-6">
                    <label for=""> Creacion:</label>
                    <br>
                    <input type="date" class="form-control" name="creacion_com" value="<?php echo $comunicadoEditar->creacion_com ?>" id="creacion_com" placeholder="Ingrese la fecha de Creacion">

                </div>


            </div>
            <br>
            <br>

            <div class="row">
                <div class="col-md-12 text-center">
                    <button type="submit" name="button" class="btn btn-primary">Guardar</button>&nbsp;
                    <a href="<?php echo site_url(); ?>/comunicados/index" class="btn btn-danger">Cancelar </a>
                </div>
            </div>
        </form>
    </div>
</div>
