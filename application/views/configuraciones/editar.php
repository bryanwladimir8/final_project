<div class="row">
    <div class="col-md-6">
        <h1>Nuevo Configuracion</h1>
    </div>
    <div>
        <a href="<?php echo site_url();?>/configuraciones/procesoActualizar">Agregar Nuevo</a>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <form action="<?php echo site_url(); ?>/configuraciones/guardar" method="post">
            <input type="text" name="id_con" id="id_con" value="<?php echo $configuracionEditar->id_con ?>" hidden/>
            <div class="row">
                <div  class="col-md-4">
                    <label for=""> Nombre:</label>
                    <br>
                    <input type="text" class="form-control"name="nombre_con" value="<?php echo $configuracionEditar->nombre_con ?>" id="nombre_con" placeholder="Ingrese el nombre">

                </div>
                <div class="col-md-4">
                    <label for="">Ruc:</label>
                    <br>
                    <input type="text" class="form-control"name="ruc_con" value="<?php echo $configuracionEditar->ruc_con ?>" id="ruc_con" placeholder="Ingrese el ruc">


                    </div>
                <div class="col-md-4">
                    <label for=""> Telefono:</label>
                    <br>
                    <input type="text" class="form-control"name="telefono_con" value="<?php echo $configuracionEditar->telefono_con ?>" id="telefono_con" placeholder="Ingrese el telefonoo">
                </div>

            </div>
            <div class="row">
                <div  class="col-md-6">
                    <label for=""> Direccion:</label>
                    <br>
                    <input type="text" class="form-control" name="direccion_con" value="<?php echo $configuracionEditar->direccion_con ?>" id="direccion_con" placeholder="Ingrese la direccion">

                </div>

                <div  class="col-md-6">
                    <label for=""> Email::</label>
                    <br>
                    <input type="text" class="form-control" name="email_con" value="<?php echo $configuracionEditar->direccion_con ?>" id="email_con" placeholder="Ingrese el Email">

                </div>
                <div  class="col-md-6">
                    <label for=""> Servidor:</label>
                    <br>
                    <input type="text" class="form-control" name="servidor_con" value="<?php echo $configuracionEditar->servidor_con ?>" id="servidor_con" placeholder="Ingrese el servidor">

                </div>
                <div  class="col-md-6">
                    <label for=""> Puerto:</label>
                    <br>
                    <input type="text" class="form-control" name="puerto_con" value="<?php echo $configuracionEditar->puerto_con ?>" id="puerto_con" placeholder="Ingrese el puerto">

                </div>
                <div  class="col-md-6">
                    <label for=""> Password:</label>
                    <br>
                    <input type="text" class="form-control" name="password_con" value="<?php echo $configuracionEditar->password_con ?>" id="password_con" placeholder="Ingrese la contraseña">

                </div>
                <div  class="col-md-6">
                    <label for=""> Creacion:</label>
                    <br>
                    <input type="date" class="form-control" name="creacion_con" value="<?php echo $configuracionEditar->creacion_con ?>" id="creacion_con" placeholder="Ingrese la fecha de creacion">

                </div>
                <div  class="col-md-6">
                    <label for=""> Actualizacion:</label>
                    <br>
                    <input type="date" class="form-control" name="actualizacion_con" value="<?php echo $configuracionEditar->actualizacion_con ?>" id="actualizacion_con" placeholder="Ingrese la fecha de actualizacion">

                </div>
                <div  class="col-md-6">
                    <label for=""> Año Inicial:</label>
                    <br>
                    <input type="date" class="form-control" name="anio_inicial_con" value="<?php echo $configuracionEditar->anio_inicial_con ?>" id="anio_inicial_con" placeholder="Ingrese la fecha inicial">

                </div>
                <div  class="col-md-6">
                    <label for=""> Mes inicial:</label>
                    <br>
                    <input type="date" class="form-control" name="mes_inicial_con" value="<?php echo $configuracionEditar->mes_inicial_con ?> " id="mes_inicial_con" placeholder="Ingrese el mes inicial">

                </div>


            </div>
            <br>
            <br>

            <div class="row">
                <div class="col-md-12 text-center">
                    <button type="submit" name="button" class="btn btn-primary">Guardar</button>&nbsp;
                    <a href="<?php echo site_url(); ?>/configuraciones/index" class="btn btn-danger">Cancelar </a>
                </div>
            </div>
        </form>
    </div>
</div>
