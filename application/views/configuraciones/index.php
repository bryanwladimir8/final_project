<div class="row">
    <div class="col-md-6">
        <h1>Listado de Configuraciones</h1>
    </div>
    <div>
        <a href="<?php echo site_url();?>/configuraciones/nuevo">Agregar Nuevo </a>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-12">
        <?php if ($configuracion): ?>
            <table class="table  table-striped" id="tablaSocios">
                <thead>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Ruc</th>
                    <th>telefono</th>
                    <th>direccion</th>
                    <th>Email</th>
                    <th>Servidor</th>
                    <th>Puerto</th>
                    <th>Password</th>
                    <th>Creqacion</th>
                    <th>Actualizacion</th>
                    <th>Pño inicial</th>
                    <th>Mes inicial</th>
                    <th>Acciones</th>
                </thead>

                <tbody>
                    <?php foreach ($configuracion as $filaTemporal): ?>
                        <tr>
                            <td><?php echo $filaTemporal->id_con ?></td>
                            <td><?php echo $filaTemporal->nombre_con ?></td>
                            <td><?php echo $filaTemporal->ruc_con ?>  </td>
                            <td><?php echo $filaTemporal->telefono_con ?></td>
                            <th><?php echo $filaTemporal->direccion_con ?></th>
                            <th><?php echo $filaTemporal->email_con ?></th>
                            <th><?php echo $filaTemporal->servidor_con ?></th>
                            <th><?php echo $filaTemporal->puerto_con ?></th>
                            <th><?php echo $filaTemporal->password_con ?></th>
                            <th><?php echo $filaTemporal->creacion_con ?></th>
                            <th><?php echo $filaTemporal->actualizacion_con ?></th>
                            <th><?php echo $filaTemporal->anio_inicial_con ?></th>
                            <th><?php echo $filaTemporal->mes_inicial_con ?></th>

                            <td class="text-center" >
                                <a href="<?php echo site_url(); ?>/configuraciones/editar/<?php echo $filaTemporal->id_con; ?>" title="Editar Configuracion" >
                                    <i class="mdi  mdi-pencil">Editar</i>
                                </a>
                                &nbsp;&nbsp;

                                <a href="<?php echo site_url(); ?>/configuraciones/eliminar/<?php echo $filaTemporal->id_con; ?>" title="Borrar Configuracion" style="color:red" onclick="return confirm('Esta seguro de borra el registro?');">
                                    <i class="mdi  mdi-close">Eliminar</i>
                                </a>

                            </td>
                        </tr>

                    <?php endforeach; ?>
                    </tbody>
                </table>

                <?php else: ?>
                <h1>No hay datos</h1>

        <?php endif; ?>


    </div>
</div>

<script type="text/javascript">
    $("#tablaSocios")
    .DataTable();
</script>
