<div class="row">
    <div class="col-md-6">
        <h1>Listado Lectura</h1>
    </div>
    <div>
        <a href="<?php echo site_url();?>/lecturas/nuevo">Agregar Nuevo </a>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-12">
        <?php if ($lectura): ?>
            <table class="table  table-striped" id="tablaLectura">
                <thead>
                    <th>ID</th>
                    <th>Año</th>
                    <th>Mes</th>
                    <th>Estado</th>
                    <th>Lectura Anterior</th>
                    <th>Lectura Actual</th>
                    <th>Fecha Creacion</th>
                    <th>Historial</th>
                    <th>Consumo</th>
                    <th>Acciones</th>
                </thead>
                
                <tbody>
                    <?php foreach ($lectura as $filaTemporal): ?>
                        <tr>
                            <td><?php echo $filaTemporal->id_lec ?></td>
                            <td><?php echo $filaTemporal->anio_lec ?></td>
                            <td><?php echo $filaTemporal->mes_lec ?> </td>
                            <td> <?php echo $filaTemporal->estado_lec ?></td>
                            <td><?php echo $filaTemporal->lectura_anterior_lec ?></td>
                            <th><?php echo $filaTemporal->fecha_creacion_lec ?></th>
                            <th><?php echo $filaTemporal->fecha_actualizacion_lec ?></th>
                            <th><?php echo $filaTemporal->fk_id_his ?></th>
                            <th><?php echo $filaTemporal->fk_id_consumo ?></th>
                            
                            <td class="text-center" >
                                <a href="<?php echo site_url(); ?>/lecturas/editar/<?php echo $filaTemporal->id_lec; ?>" title="Editar Lectura" >
                                    <i class="mdi  mdi-pencil">Editar</i>
                                </a>
                                &nbsp;&nbsp;

                                <a href="<?php echo site_url(); ?>/lecturas/eliminar/<?php echo $filaTemporal->id_lec; ?>" title="Borrar Lectura" style="color:red" onclick="return confirm('Esta seguro de borra el registro?');">
                                    <i class="mdi  mdi-close">Eliminar</i>
                                </a>
            
                            </td>
                        </tr>         

                    <?php endforeach; ?>
                    </tbody>
                </table>

                <?php else: ?>
                <h1>No hay datos</h1>

        <?php endif; ?>

       
    </div>
</div>

<script type="text/javascript">
    $("#tablaLectura")
    .DataTable();
</script>