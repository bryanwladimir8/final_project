<h1>Listado de Tipo Evento</h1>
<br>
<br>
<div class="row">
    <div class="col-md-12">
        <?php if ($tipo): ?>
            <table class="table  table-striped" id="tablaDetalle">
                <thead>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Estado</th>
                    <th>Creacion </th>
                    <th>Actualizacion</th>
                    <th>Acciones</th>
                </thead>

                <tbody>
                    <?php foreach ($tipo as $filaTemporal): ?>
                        <tr>
                            <td><?php echo $filaTemporal->id_te ?></td>
                            <td><?php echo $filaTemporal->nombre_te?></td>
                            <td><?php echo $filaTemporal->estado_te ?>  </td>
                            <td> <?php echo $filaTemporal->creacion_te ?></td>
                            <th><?php echo $filaTemporal->actualizacion_te ?></th>

                            <td class="text-center" >
                                <a href="<?php echo site_url(); ?>/tipos/editar/<?php echo $filaTemporal->id_te; ?>" title="Editar Detalle" >
                                    <i class="mdi  mdi-pencil">Editar</i>
                                </a>
                                &nbsp;&nbsp;

                                <a href="<?php echo site_url(); ?>/tipos/eliminar/<?php echo $filaTemporal->id_te; ?>" title="Borrar Detalle" style="color:red" onclick="return confirm('Esta seguro de borra el registro?');">
                                    <i class="mdi  mdi-close">Eliminar</i>
                                </a>

                            </td>
                        </tr>

                    <?php endforeach; ?>
                    </tbody>
                </table>

                <?php else: ?>
                <h1>No hay datos</h1>

        <?php endif; ?>


    </div>
</div>

<script type="text/javascript">
    $("#tablaDetalle")
    .DataTable();
</script>
