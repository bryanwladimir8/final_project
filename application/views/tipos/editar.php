<div class="row">
    <div class="col-md-6">
        <h1>Nuevo Tipo evento</h1>
    </div>
    <div>
        <a href="<?php echo site_url();?>/tipos/procesoActualizar">Agregar Nuevo</a>
    </div>
</div>



<div class="row">
    <div class="col-md-12">
        <form action="<?php echo site_url(); ?>/tipos/guardar" method="post">
            <input type="text" name="id_te" id="id_te" value="<?php echo $tipoEditar->id_te ?>" hidden/>
            <div class="row">
                <div  class="col-md-4">
                    <label for=""> Nombre:</label>
                    <  <br>
                      <input type="text" class="form-control"name="nombre_te" value="<?php echo $tipoEditar->nombre_te ?>" id="nombre_te" placeholder="Ingrese el nombre">

            </div>
            <div class="row">
                <div  class="col-md-6">
                    <label for=""> Estado:</label>
                    <br>
                    <input type="text" class="form-control" name="estado_te" value="<?php echo $tipoEditar->estado_te ?>" id="estado_te" placeholder="Ingrese el estado">

                </div>


                <div  class="col-md-6">
                    <label for=""> Creacion:</label>
                    <br>
                    <input type="date" class="form-control" name="creacion_te" value="<?php echo $tipoEditar->creacion_te ?>" id="creacion_te" placeholder="Ingrese la fecha de creacion">

                </div>
                <div  class="col-md-6">
                    <label for=""> Actualizacion:</label>
                    <br>
                    <input type="date" class="form-control" name=" actualizacion_te " value="<?php echo $tipoEditar->actualizacion_te ?>" id="actualizacion_te" placeholder="Ingrese la fecha de actualizacion">

                </div>

            </div>
            <br>
            <br>

            <div class="row">
                <div class="col-md-12 text-center">
                    <button type="submit" name="button" class="btn btn-primary">Guardar</button>&nbsp;
                    <a href="<?php echo site_url(); ?>/tipos/index" class="btn btn-danger">Cancelar </a>
                </div>
            </div>
        </form>
    </div>
</div>
