<div class="container-fluid">
    <div class="card">
        <center>
            <h1>
                <b>
                    FORMULARIO EDITAR EXCEDENTE
                </b>
            </h1>
        </center>
        <div class="card-body">
            <form action="<?php echo site_url('/Excedentes/EditarExecedente') ?>" method="post" id="frm_nuevo_usuario">
            <div class="col-4">
                        <div class="mb-3">
                            <input type="hidden"  value="<?php echo $excedenteEditar->id_ex ?>" type="text" class="form-control" name="id_ex" id="id_ex" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="" class="form-label">ID TARIFA</label>
                            <select class="form-select form-select-lg" name="id_tar" id="id_tar">
                                <?php foreach ($tarifa as $temporal){ ?>
                                    <option value="<?php echo $temporal->id_tar?>"><?php echo $temporal->nombre_tar ?></option>
                                <?php } ?>
                            </select>
                     </div>
                    </div>
    
                 

                    <div class="col-4">
                        <div class="mb-3">
                            <label for="nombre_usu" class="form-label">LIMITE MINIMO :</label>
                            <input value="<?php echo $usuarioEditar->nombre_usu ?>" type="text" class="form-control" name="nombre_usu" id="nombre_usu" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="email_usu" class="form-label">LIMITE MAXIMO:</label>
                            <input value="<?php echo $usuarioEditar->email_usu ?>" type="text" class="form-control" name="email_usu" id="email_usu" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="password_usu" class="form-label">TARIFA:</label>
                            <input value="<?php echo $usuarioEditar->password_usu ?>" type="text" class="form-control" name="password_usu" id="password_usu" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="estado_usu" class="form-label">FECHA ACTUALIZACION:</label>
                            <input value="<?php echo $usuarioEditar->estado_usu ?>" type="datetime-local" class="form-control" name="estado_usu" id="estado_usu" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="estado_usu" class="form-label">FECHA CREACION:</label>
                            <input value="<?php echo $usuarioEditar->estado_usu ?>" type="datetime-local" class="form-control" name="estado_usu" id="estado_usu" aria-describedby="helpId" placeholder="">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="" class="form-label">PERFIL</label>
                            <select class="form-select form-select-lg" name="fk_id_per" id="fk_id_per">
                                <?php foreach ($perfil as $temporal){ ?>
                                    <option value="<?php echo $temporal->id_per?>"><?php echo $temporal->nombre_per ?></option>
                                <?php } ?>
                            </select>
                     </div>
                    </div>

                    


                    <center>
                        <button  class="btn btn-primary" type="submit">Guardar</button>
                        <a name="" id="" class="btn btn-dark" href="<?php echo site_url('/Usuarios/index') ?>" role="button">Cancelar</a>
                    </center>
            </form>
        </div>
    </div>
</div>
</div>
</div>
<script type="text/javascript">
    $("#frm_nuevo_usuario").validate({
        rules: {
            apellido_usu: {
                required: true,
                minlength: 10,
                maxlength: 10,
                digits: true
            },
            nombre_usu: {
                required: true,

                letras: true
            },
            email_usu: {
                required: true,

                letras: true
            },
            


        },
        messages: {
            apellido_usu: {
                required: "Nesesariamente debe elegir una",
                minlength: "Cedula incorrecta",
                maxlength: "Cedula incorrecta 10 digitos",
                digits: "Este campo solo acepta numeros",
                number: "Este campo solo acepta numeros",
            },
            nombre_usu: {
                required: "Porfavor ingrese la descripcion del delito",
                // minlength: "El apellido debe tener minimo 3 digitos ",
                // maxlength: "APellido incorrecto"
            },
            email_usu: {
                required: "debe ingresar el tiempo de condena para el delito",
                // minlength: "El nombre debe tener 3 dígitos",
                // maxlength: "Nombres incorrectos 50 dígitos",
            },
            


        }
    });
</script>

</div>