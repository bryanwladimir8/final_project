<div class="row">
    <div class="col-md-6">
        <h1>Listado de Asistencias</h1>
    </div>
    <div>
        <a href="<?php echo site_url();?>/asistencias/nuevo">Agregar Nuevo </a>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-12">
        <?php if ($asistencia): ?>
            <table class="table  table-striped" id="tablaAsistencias">
                <thead>
                    <th>ID</th>
                    <th>FK ID EVE</th>
                    <th>FK ID Socio</th>
                    <th>Tipo Asistencia</th>
                    <th>Valor Asistencia</th>
                    <th>Atraso Asistencia </th>
                    <th>Valor Atraso Asistencia</th>
                    <th>Creacio Asistencia</th>
                    <th>Actualizaciom Asistencia</th>
                    <th>Acciones</th>
                </thead>

                <tbody>
                    <?php foreach ($asistencia as $filaTemporal): ?>
                        <tr>
                            <td><?php echo $filaTemporal->id_asi ?></td>
                            <td><?php echo $filaTemporal->fk_id_eve ?></td>
                            <td><?php echo $filaTemporal->fk_id_soc ?>  </td>
                            <td><?php echo $filaTemporal->tipo_asi ?></td>
                            <td><?php echo $filaTemporal->valor_asi ?></td>
                            <th><?php echo $filaTemporal->atraso_asi ?></th>
                            <th><?php echo $filaTemporal->valor_atraso_asi ?></th>
                            <th><?php echo $filaTemporal->creacion_asi ?></th>
                            <th><?php echo $filaTemporal->actualizacion_asi ?></th>

                            <td class="text-center" >
                                <a href="<?php echo site_url(); ?>/asistencias/editar/<?php echo $filaTemporal->id_asi; ?>" title="Editar Asistencia" >
                                    <i class="mdi  mdi-pencil">Editar</i>
                                </a>
                                &nbsp;&nbsp;

                                <a href="<?php echo site_url(); ?>/asistencias/eliminar/<?php echo $filaTemporal->id_asi; ?>" title="Borrar Asistencia" style="color:red" onclick="return confirm('Esta seguro de borra el registro?');">
                                    <i class="mdi  mdi-close">Eliminar</i>
                                </a>

                            </td>
                        </tr>

                    <?php endforeach; ?>
                    </tbody>
                </table>

                <?php else: ?>
                <h1>No hay datos</h1>

        <?php endif; ?>


    </div>
</div>

<script type="text/javascript">
    $("#tablaAsistencias")
    .DataTable();
</script>
