<div class="row">
    <div class="col-md-6">
        <h1>Nuevo Asistencia</h1>
    </div>
    <div>
        <a href="<?php echo site_url();?>/asistencias/nuevo">Agregar Nuevo </a>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <form action="<?php echo site_url(); ?>/asistencias/guardar" method="post">

            <div class="row">
                <div  class="col-md-4">
                    <label for=""> FK ID EVE:</label>
                    <br>
                    <input type="text" class="form-control"name="fk_id_eve" value="" id="fk_id_eve" placeholder="Ingrese el año">

                </div>
                <div class="col-md-4">
                    <label for="">FK ID SOC:</label>
                    <br>
                    <input type="text" class="form-control"name="fk_id_soc" value="" id="fk_id_soc" placeholder="Ingrese el mes">


                    </div>
                <div class="col-md-4">
                    <label for=""> Tipo Asistencia:</label>
                    <br>
                    <input type="text" class="form-control"name="tipo_asi" value="" id="tipo_asi" placeholder="Ingrese el tipo de asistencia">
                </div>



            </div>
            <div class="row">
                <div  class="col-md-6">
                    <label for=""> Valor Asistencia:</label>
                    <br>
                    <input type="text" class="form-control" name="valor_asi" value="" id="valor_asi" placeholder="Ingrese el valor de la asistencia">

                </div>
                <div class="col-md-4">
                    <label for=""> Atraso Asistencia:</label>
                    <br>
                    <input type="text" class="form-control"name="atraso_asi" value="<?php echo $asistenciaEditar->atraso_asi ?>" id="atraso_asi" placeholder="Ingrese el atraso de asistencia">
                </div>

                <div  class="col-md-6">
                    <label for="">Valor Atraso Asistencia:</label>
                    <br>
                    <input type="date" class="form-control" name="valor_atraso_asi" value="" id="valor_atraso_asi" placeholder="Ingrese la lectura actual">

                </div>
                <div  class="col-md-6">
                    <label for="">Creaciom Asistencia:</label>
                    <br>
                    <input type="date" class="form-control" name="creacion_asi" value="" id="creacion_asi" placeholder="Ingrese la lectura actual">

                </div>
                <div  class="col-md-6">
                    <label for="">Actualizaciom Asistencia:</label>
                    <br>
                    <input type="date" class="form-control" name="actualizacion_asi" value="" id="actualizacion_asi" placeholder="Ingrese la lectura actual">

                </div>



            </div>
            <br>
            <br>

            <div class="row">
                <div class="col-md-12 text-center">
                    <button type="submit" name="button" class="btn btn-primary">Guardar</button>&nbsp;
                    <a href="<?php echo site_url(); ?>/asistencias/index" class="btn btn-danger">Cancelar </a>
                </div>
            </div>
        </form>
    </div>
</div>
