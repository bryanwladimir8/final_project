<h1>Nuevo Asistencia</h1>
<div class="row">
    <div class="col-md-12">
        <form action="<?php echo site_url(); ?>/asistencias/procesoActualizar" method="post">
            <input type="text" id="id_asi" name="id_asi" value="<?php echo $asistenciaEditar->id_asi?>" hidden>
<div class="row">
    <div  class="col-md-4">
      <label for=""> FK ID EVE:</label>
      <br>
      <select name="fk_id_eve" id="fk_id_eve" class="form-control">
          <?php  foreach ($usuario as $t) { ?>
              <option value="<?= $t->id_usu?>"><?= $t->nombre_usu?></option>
          <?php } ?>
      </select>
  </div>
  <div class="col-md-3">
      <label for="">FK ID SOC:</label>
      <br>
      <select name="fk_id_soc" id="fk_id_soc" class="form-control">
          <?php  foreach ($recaudacion as $t) { ?>
              <option value="<?= $t->id_rec?>"><?= $t->id_rec?></option>
          <?php } ?>
      </select>

      </div>
    <div class="col-md-4">
        <label for=""> Tipo Asistencia:</label>
        <br>
        <input type="text" class="form-control"name="tipo_asi" value="<?php echo $asistenciaEditar->tipo_asi ?>" id="tipo_asi" placeholder="Ingrese el estado">
    </div>



</div>
<div class="row">
    <div  class="col-md-6">
        <label for=""> Valor Asistencia:</label>
        <br>
        <input type="text" class="form-control" name="valor_asi" value="<?php echo $asistenciaEditar->valor_asi ?>" id="valor_asi" placeholder="Ingrese la lectura actual">

    </div>
    <div class="col-md-4">
        <label for=""> Atraso Asistencia:</label>
        <br>
        <input type="text" class="form-control"name="atraso_asi" value="<?php echo $asistenciaEditar->atraso_asi ?>" id="atraso_asi" placeholder="Ingrese el estado">
    </div>

    <div  class="col-md-6">
        <label for="">Valor Atraso Asistencia:</label>
        <br>
        <input type="text" class="form-control" name="valor_atraso_asi" value="<?php echo $asistenciaEditar->valor_atraso_asi ?>" id="valor_atraso_asi" placeholder="Ingrese la lectura actual">

    </div>
    <div  class="col-md-6">
        <label for="">Creaciom Asistencia:</label>
        <br>
        <input type="date" class="form-control" name="creacion_asi" value="<?php echo $asistenciaEditar->creacion_asi ?>" id="creacion_asi" placeholder="Ingrese la lectura actual">

    </div>
    <div  class="col-md-6">
        <label for="">Actualizaciom Asistencia:</label>
        <br>
        <input type="date" class="form-control" name="actualizacion_asi" value="<?php echo $asistenciaEditar->actualizacion_asi ?>" id="actualizacion_asi" placeholder="Ingrese la lectura actual">

    </div>



</div>


            </div>
            <br>
            <br>

            <div class="row">
                <div class="col-md-12 text-center">
                    <button type="submit" name="button" class="btn btn-primary">Guardar</button>&nbsp;
                    <a href="<?php echo site_url(); ?>/asistencias/index" class="btn btn-danger">Cancelar </a>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    $("#fk_id_soc").val('<?php echo $asistenciaEditar->fk_id_soc; ?>');

</script>
