<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Medidor extends CI_Model {
    public function __construct() {
    parent::__construct();
    }

    public function insertar($datos){
        return $this->db->insert("medidor",$datos);
    }

    public function obtenerTodos(){
        $listadoMedidor=
        $this->db->get("medidor");
        if($listadoMedidor
           ->num_rows()>0){//Si hay datos
           return $listadoMedidor->result();
        }else{//No hay datos
           return false;
        }
    }

    public function eliminar($id_med){
        $this->db->where("id_med",$id_med);
  	return $this->db->delete("medidor");
    }

    public function obtenerPorId($id_med){
        $this->db->where("id_med",$id_med);
        $medidor=$this->db->get("medidor");
        if($medidor->num_rows()>0){
          return $medidor->row();
        }
        return false;
    }

    public function actualizar($id_med,$datos)
    {
      $this->db->where("id_med",$id_med);
      return $this->db->update("medidor",$datos);
    }

}
