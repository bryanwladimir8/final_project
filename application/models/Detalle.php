<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Detalle extends CI_Model {
    public function __construct() {
    parent::__construct(); 
    }

    public function insertar($datos){
        return $this->db->insert("detalle",$datos);
    }

    public function obtenerTodos(){
        $listadoDetalle=
        $this->db->get("detalle");
        if($listadoDetalle
           ->num_rows()>0){//Si hay datos
           return $listadoDetalle->result();
        }else{//No hay datos
           return false;
        } 
    }

    public function eliminar($id_det){
        $this->db->where("id_det",$id_det);
  	return $this->db->delete("detalle");
    }

    public function obtenerPorId($id_det){
        $this->db->where("id_det",$id_det);
        $detalle=$this->db->get("detalle");
        if($detalle->num_rows()>0){
          return $detalle->row();
        }
        return false;  
    }

    public function actualizar($id_det,$datos)
    {
      $this->db->where("id_det",$id_det);
      return $this->db->update("detalle",$datos);
    }

}