<?php 
class Ruta extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    public function InsertarRuta($datosRuta){
        return $this->db->insert('ruta',$datosRuta);
    }
    public function ObtenerRutas(){
        $datosRutas = $this->db->get('ruta');
        if ($datosRutas->num_rows()>0) {
            return $datosRutas->result();
        } else {
            return false;
        }
        
    }
    function obtenerRuta($id_rut){
        $this->db->where("id_rut",$id_rut);
        $ruta = $this->db->get("ruta");
        if ($ruta->num_rows()) {
            return $ruta->row();

        }
        return false;

    }
    public function actualizarRuta($id_rut,$datos){
        $this->db->where('id_rut',$id_rut);
        return $this->db->update("ruta",$datos);
    }
    public function borrarRuta($id_rut){
        $this->db->where('id_rut',$id_rut);
        return $this->db->delete('ruta');
        
    }

}

?>