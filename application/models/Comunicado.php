<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Comunicado extends CI_Model {
    public function __construct() {
    parent::__construct();
    }

    public function insertar($datos){
        return $this->db->insert("comunicado",$datos);
    }

    public function obtenerTodos(){
        $listadoComunicado=
        $this->db->get("comunicado");
        if($listadoComunicado
           ->num_rows()>0){//Si hay datos
           return $listadoComunicado->result();
        }else{//No hay datos
           return false;
        }
    }

    public function eliminar($id_com){
        $this->db->where("id_com",$id_com);
  	return $this->db->delete("comunicado");
    }

    public function obtenerPorId($id_com){
        $this->db->where("id_com",$id_com);
        $comunicado=$this->db->get("comunicado");
        if($comunicado->num_rows()>0){
          return $comunicado->row();
        }
        return false;
    }
    public function actualizar($id_com,$datos)
    {
      $this->db->where("id_com",$id_com);
      return $this->db->update("comunicado",$datos);
    }

}
