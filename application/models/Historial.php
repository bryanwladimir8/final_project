<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Historial extends CI_Model {
    public function __construct() {
    parent::__construct(); 
    }

    public function insertar($datos){
        return $this->db->insert("historial_propietario",$datos);
    }

    public function obtenerTodos(){
        $listadoHistorial=
        $this->db->get("historial_propietario");
        if($listadoHistorial
           ->num_rows()>0){//Si hay datos
           return $listadoHistorial->result();
        }else{//No hay datos
           return false;
        } 
    }

    public function eliminar($id_his){
        $this->db->where("id_his",$id_his);
  	return $this->db->delete("historial_propietario");
    }

    public function obtenerPorId($id_his){
        $this->db->where("id_his",$id_his);
        $consumo=$this->db->get("historial_propietario");
        if($consumo->num_rows()>0){
          return $consumo->row();
        }
        return false;  
    }

    public function actualizar($id_his,$datos)
    {
      $this->db->where("id_his",$id_his);
      return $this->db->update("historial_propietario",$datos);
    }

}