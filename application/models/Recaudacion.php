<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Recaudacion extends CI_Model {
    public function __construct() {
    parent::__construct(); 
    }

    public function insertar($datos){
        return $this->db->insert("recaudacion",$datos);
    }

    public function obtenerTodos(){
        $listadoRecaudacion=
        $this->db->get("recaudacion");
        if($listadoRecaudacion
           ->num_rows()>0){//Si hay datos
           return $listadoRecaudacion->result();
        }else{//No hay datos
           return false;
        } 
    }

    public function eliminar($id_rec){
        $this->db->where("id_rec",$id_rec);
  	return $this->db->delete("recaudacion");
    }

    public function obtenerPorId($id_rec){
        $this->db->where("id_rec",$id_rec);
        $socio=$this->db->get("recaudacion");
        if($socio->num_rows()>0){
          return $socio->row();
        }
        return false;  
    }

    public function actualizar($id_rec,$datos)
    {
      $this->db->where("id_rec",$id_rec);
      return $this->db->update("recaudacion",$datos);
    }

}