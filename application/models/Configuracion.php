<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Configuracion extends CI_Model {
    public function __construct() {
    parent::__construct();
    }

    public function insertar($datos){
        return $this->db->insert("configuracion",$datos);
    }

    public function obtenerTodos(){
        $listadoConfiguracion=
        $this->db->get("configuracion");
        if($listadoConfiguracion
           ->num_rows()>0){//Si hay datos
           return $listadoConfiguracion->result();
        }else{//No hay datos
           return false;
        }
    }

    public function eliminar($id_con){
        $this->db->where("id_con",$id_con);
  	return $this->db->delete("configuracion");
    }

    public function obtenerPorId($id_con){
        $this->db->where("id_con",$id_con);
        $configuracion=$this->db->get("configuracion");
        if($configuracion->num_rows()>0){
          return $configuracion->row();
        }
        return false;
    }
    public function actualizar($id_con,$datos)
    {
      $this->db->where("id_con",$id_con);
      return $this->db->update("configuracion",$datos);
    }

}
