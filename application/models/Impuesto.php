<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Impuesto extends CI_Model {
    public function __construct() {
    parent::__construct();
    }

    public function insertar($datos){
        return $this->db->insert("impuesto",$datos);
    }

    public function obtenerTodos(){
        $listadoImpuesto=
        $this->db->get("impuesto");
        if($listadoImpuesto
           ->num_rows()>0){//Si hay datos
           return $listadoImpuesto->result();
        }else{//No hay datos
           return false;
        }
    }

    public function eliminar($id_con){
        $this->db->where("id_con",$id_con);
  	return $this->db->delete("impuestos");
    }

    public function obtenerPorId($id_con){
        $this->db->where("id_con",$id_con);
        $configuracion=$this->db->get("configuracion");
        if($configuracion->num_rows()>0){
          return $configuracion->row();
        }
        return false;
    }
    public function actualizar($id_con,$datos)
    {
      $this->db->where("id_con",$id_con);
      return $this->db->update("configuracion",$datos);
    }

}
