<?php 
class Excedente extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    public function InsertarExcedente($datosExcedente){
        return $this->db->insert('excedente',$datosExcedente);
    }
    public function ObtenerExcedentes(){
        $datosExcedentes = $this->db->get('excedente');
        if ($datosExcedentes->num_rows()>0) {
            return $datosExcedentes->result();
        } else {
            return false;
        }
        
    }
    function obtenerExcedente($id_ex){
        $this->db->where("id_ex",$id_ex);
        $excedente = $this->db->get("excedente");
        if ($excedente->num_rows()) {
            return $excedente->row();

        }
        return false;

    }
    public function actualizarExcedente($id_ex,$datos){
        $this->db->where('id_ex',$id_ex);
        return $this->db->update("excedente",$datos);
    }
    public function borrarExcedente($id_ex){
        $this->db->where('id_ex',$id_ex);
        return $this->db->delete('excedente');
        
    }

}

?>