<?php 
class Tarifa extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    public function InsertarTarifa($datosTarifa){
        return $this->db->insert('tarifa',$datosTarifa);
    }
    public function ObtenerTarifas(){
        $datosTarifas = $this->db->get('tarifa');
        if ($datosTarifas->num_rows()>0) {
            return $datosTarifas->result();
        } else {
            return false;
        }
        
    }
    function obtenerTarifa($id_tar){
        $this->db->where("id_tar",$id_tar);
        $tarifa = $this->db->get("tarifa");
        if ($tarifa->num_rows()) {
            return $tarifa->row();

        }
        return false;

    }
    public function actualizarTarifa($id_tar,$datos){
        $this->db->where('id_tar',$id_tar);
        return $this->db->update("tarifa",$datos);
    }
    public function borrarTarifa($id_tar){
        $this->db->where('id_tar',$id_tar);
        return $this->db->delete('tarifa');
        
    }

}

?>