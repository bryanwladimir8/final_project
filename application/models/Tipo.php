<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tipo extends CI_Model {
    public function __construct() {
    parent::__construct();
    }

    public function insertar($datos){
        return $this->db->insert("tipo_evento",$datos);
    }

    public function obtenerTodos(){
        $listadoTipo=
        $this->db->get("tipo_evento");
        if($listadoTipo
           ->num_rows()>0){//Si hay datos
           return $listadoTipo->result();
        }else{//No hay datos
           return false;
        }
    }

    public function eliminar($id_te){
        $this->db->where("id_te",$id_te);
  	return $this->db->delete("tipo_evento");
    }

    public function obtenerPorId($id_te){
        $this->db->where("id_te",$id_te);
        $tipo=$this->db->get("tipo_evento");
        if($tipo->num_rows()>0){
          return $tipo->row();
        }
        return false;
    }
    public function actualizar($id_te,$datos)
    {
      $this->db->where("id_te",$id_te);
      return $this->db->update("tipo",$datos);
    }

}
