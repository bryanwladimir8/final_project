<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Asistencia extends CI_Model {
    public function __construct() {
    parent::__construct();
    }

    public function insertar($datos){
        return $this->db->insert("asistencia",$datos);
    }

    public function obtenerTodos(){
        $listadoAsistencia=
        $this->db->get("asistencia");
        if($listadoAsistencia
           ->num_rows()>0){//Si hay datos
           return $listadoAsistencia->result();
        }else{//No hay datos
           return false;
        }
    }

    public function eliminar($id_asi){
        $this->db->where("id_asi",$id_asi);
  	return $this->db->delete("asistencia");
    }

    public function obtenerPorId($id_asi){
        $this->db->where("id_asi",$id_asi);
        $asistencia=$this->db->get("asistencia");
        if($asistencia->num_rows()>0){
          return $asistencia->row();
        }
        return false;
    }

    public function actualizar($id_asi,$datos)
    {
      $this->db->where("id_asi",$id_asi);
      return $this->db->update("asistencia",$datos);
    }

}
