<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Recaudaciones extends CI_Controller {

	public function __construct() {
		parent::__construct(); 
		$this->load->model("Socio"); 
        $this->load->model("Recaudacion");  
	}

	public function index()
	{
		$data["recaudaciones"]=$this->Recaudacion->obtenerTodos();
		$this->load->view('header');
		$this->load->view('recaudaciones/index',$data);
		$this->load->view('footer');
	}

    public function nuevo()
	{
        $data["socios"]=$this->Socio->obtenerTodos();
		$this->load->view('header');
		$this->load->view('recaudaciones/nuevo',$data);
		$this->load->view('footer');
	}

    public function editar($id_rec)
	{
        $data["socios"]=$this->Socio->obtenerTodos();
        $data["recaudacionEditar"] = $this->Recaudacion->obtenerPorId($id_rec);
		$this->load->view('header');
		$this->load->view('recaudaciones/editar',$data);
		$this->load->view('footer');
	}

	public function guardar(){
		$datos = array(
            "numero_factura_rec"=>$this->input->post("numero_factura_rec"),
			"numero_autorizacion_rec"=>$this->input->post("numero_autorizacion_rec"),
            "identificacion_rec"=>$this->input->post("identificacion_rec"),
			"fecha_hora_autorizacion_rec"=>$this->input->post("fecha_hora_autorizacion_rec"),
            "ambiente_rec"=>$this->input->post("ambiente_rec"),
            "emision_rev"=> $this->input->post("emision_rev"),
            "clave_acceso_rec"=>$this->input->post("clave_acceso_rec"),
            "email_rec"=>$this->input->post("email_rec"),
            "observacion_rec"=>$this->input->post("observacion_rec"),
            "fk_id_soc"=>$this->input->post("fk_id_soc"),
            "nombre_rec"=>$this->input->post("nombre_rec"),
            "identificacion_rec"=>$this->input->post("identificacion_rec"),
            "direccion_rec"=>$this->input->post("direccion_rec"),
            "fecha_emision_rec"=>$this->input->post("fecha_emision_rec"),
            "estado_rec"=>$this->input->post("estado_rec"),
		);
		//validamos los campos del formulario 
		if ($this->Recaudacion->insertar($datos))
		{
			//llamamos la libraria session user_data solo para una vez y flash_data para varias
			
		}else {
			
		}
		redirect ('recaudaciones/index');

	}

	public function eliminar($id_rec){
	
		  if ($this->Recaudacion->eliminar($id_rec)) {
			
		  } else {
			
	 
		  }redirect ('recaudaciones/index');
	}

    public function procesoActualizar(){
        $datos = array(
            "numero_factura_rec"=>$this->input->post("numero_factura_rec"),
			"numero_autorizacion_rec"=>$this->input->post("numero_autorizacion_rec"),
            "identificacion_rec"=>$this->input->post("identificacion_rec"),
			"fecha_hora_autorizacion_rec"=>$this->input->post("fecha_hora_autorizacion_rec"),
            "ambiente_rec"=>$this->input->post("ambiente_rec"),
            "emision_rev"=> $this->input->post("emision_rev"),
            "clave_acceso_rec"=>$this->input->post("clave_acceso_rec"),
            "email_rec"=>$this->input->post("email_rec"),
            "observacion_rec"=>$this->input->post("observacion_rec"),
            "fk_id_soc"=>$this->input->post("fk_id_soc"),
            "nombre_rec"=>$this->input->post("nombre_rec"),
            "identificacion_rec"=>$this->input->post("identificacion_rec"),
            "direccion_rec"=>$this->input->post("direccion_rec"),
            "fecha_emision_rec"=>$this->input->post("fecha_emision_rec"),
            "estado_rec"=>$this->input->post("estado_rec"),

		);
        $id_rec=$this->input->post("id_rec");
		//validamos los campos del formulario 
		if ($this->Recaudacion->actualizar($id_rec,$datos))
		{
			//llamamos la libraria session user_data solo para una vez y flash_data para varias
			
		}else {
			
		}
		redirect ('recaudaciones/index');

    }
}
