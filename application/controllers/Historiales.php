<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Historiales extends CI_Controller {

	public function __construct() {
		parent::__construct(); 
		
        $this->load->model("Historial");  
        $this->load->model("Socio");    
	}

	public function index()
	{
		$data["historial"]=$this->Historial->obtenerTodos();
		$this->load->view('header');
		$this->load->view('historiales/index',$data);
		$this->load->view('footer');
	}

    public function nuevo()
	{
        $data["socios"] = $this->Socio->obtenerTodos();
		$this->load->view('header');
		$this->load->view('historiales/nuevo',$data);
		$this->load->view('footer');
	}

    public function editar($id_his)
	{
        $data["socios"] = $this->Socio->obtenerTodos();
        $data["historialEditar"] = $this->Historial->obtenerPorId($id_his);
		$this->load->view('header');
		$this->load->view('historiales/editar',$data);
		$this->load->view('footer');
	}

	public function guardar(){
		$datos = array(
            "fk_id_med"=>$this->input->post("fk_id_med"),
			"fk_id_soc"=>$this->input->post("fk_id_soc"),
            "estado_his"=>$this->input->post("estado_his"),
			"observacion_his"=>$this->input->post("observacion_his"),
            "propietario_actual_his"=>$this->input->post("propietario_actual_his"),
        
		);
		//validamos los campos del formulario 
		if ($this->Historial->insertar($datos))
		{
			//llamamos la libraria session user_data solo para una vez y flash_data para varias
			
		}else {
			
		}
		redirect ('historiales/index');

	}

	public function eliminar($id_his){
	
		  if ($this->Historial->eliminar($id_his)) {
			
		  } else {
			
	 
		  }redirect ('historiales/index');
	}

    public function procesoActualizar(){
        $datos = array(
            "fk_id_med"=>$this->input->post("fk_id_med"),
			"fk_id_soc"=>$this->input->post("fk_id_soc"),
            "estado_his"=>$this->input->post("estado_his"),
			"observacion_his"=>$this->input->post("observacion_his"),
            "propietario_actual_his"=>$this->input->post("propietario_actual_his"),

		);
        $id_his=$this->input->post("id_his");
		//validamos los campos del formulario 
		if ($this->Historial->actualizar($id_his,$datos))
		{
			//llamamos la libraria session user_data solo para una vez y flash_data para varias
			
		}else {
			
		}
		redirect ('historiales/index');

    }
}
