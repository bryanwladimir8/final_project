<?php 
class Rutas extends CI_Controller{

    function __construct(){
        parent::__construct();
        $this->load->model('Ruta');
    }
    public function index(){
        $datos['rutasIndex']=$this->Ruta->ObtenerRutas();
        $this->load->view('header');
        $this->load->view('/rutas/index',$datos);
        $this->load->view('footer');
    }
    public function nuevo(){
        $this->load->view('header');
        $this->load->view('/rutas/nuevo');
        $this->load->view('footer');
    }
    public function editar($id_rut){
        $datos['rutaEditar']=$this->Ruta->obtenerRuta($id_rut);
        $this->load->view('header');
        $this->load->view('/rutas/editar',$datos);
        $this->load->view('footer');
    }
    public function GuardarRuta(){
        $datosRuta=array(
			"nombre_rut"=>$this->input->post('nombre_rut'),
			"descripcion_rut"=>$this->input->post('descripcion_rut'),
      		"estado_rut"=>$this->input->post('estado_rut')
      		
		);
        if ($this->Ruta->InsertarRuta($datosRuta)) {

        } else {

        }
        redirect("/Rutas/index");
        
    }
    public function EditarRuta(){
        $nuevosDatos=array(
            "nombre_rut"=>$this->input->post('nombre_rut'),
			"descripcion_rut"=>$this->input->post('descripcion_rut'),
      		"estado_rut"=>$this->input->post('estado_rut')
        );
        // print_r($nuevosDatos);
        $id_rut = $this->input->post('id_rut');
        if ($this->Ruta->actualizarRuta($id_rut,$nuevosDatos)) {
            redirect("/Rutas/index");
        } else {
            echo "No se pudo actualizar";
        }
        

    }
    public function Eliminar($id_rut){
        if ($this->Ruta->borrarRuta($id_rut)) {

        } else {

        }
        redirect("/Rutas/index");
        
}  
}





?>