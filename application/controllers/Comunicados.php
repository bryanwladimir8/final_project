<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Comunicados extends CI_Controller {

	public function __construct() {
		parent::__construct();

        $this->load->model("Comunicado");

	}

	public function index()
	{
		$data["comunicado"]=$this->Comunicado->obtenerTodos();
		$this->load->view('header');
		$this->load->view('comunicados/index',$data);
		$this->load->view('footer');
	}

    public function nuevo()
	{

		$this->load->view('header');
		$this->load->view('comunicados/nuevo');
		$this->load->view('footer');
	}

    public function editar($id_com)
	{

        $data["comunicadoEditar"] = $this->Comunicado->obtenerPorId($id_com);
		$this->load->view('header');
		$this->load->view('comunicados/editar',$data);
		$this->load->view('footer');
	}

	public function guardar(){
		$datos = array(
				"fecha_com"=>$this->input->post("fecha_com"),
			  "mensaje_com"=>$this->input->post("mensaje_com"),
				"actualizacion_com"=>$this->input->post("actualizacion_com"),
				"creacion_com"=>$this->input->post("creacion_com"),
		);
		//validamos los campos del formulario
		if ($this->Comunicado->insertar($datos))
		{
			//llamamos la libraria session user_data solo para una vez y flash_data para varias

		}else {

		}
		redirect ('comunicados/index');

	}

	public function eliminar($id_com){

		  if ($this->Comunicado->eliminar($id_com)) {

		  } else {


		  }redirect ('comunicados/index');
	}

    public function procesoActualizar(){
        $datos = array(
					"fecha_com"=>$this->input->post("fecha_com"),
					"mensaje_com"=>$this->input->post("mensaje_com"),
					"actualizacion_com"=>$this->input->post("actualizacion_com"),
					"retencion_imp"=>$this->input->post("retencion_imp"),
					"creacion_com"=>$this->input->post("creacion_com"),

		);
        $id_com=$this->input->post("id_com");
		//validamos los campos del formulario
		if ($this->Comunicado->actualizar($id_com,$datos))
		{
			//llamamos la libraria session user_data solo para una vez y flash_data para varias

		}else {

		}
		redirect ('comunicados/index');

    }
}
