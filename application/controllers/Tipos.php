<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tipos extends CI_Controller {

	public function __construct() {
		parent::__construct();

        $this->load->model("Tipo");

	}

	public function index()
	{
		$data["tipo"]=$this->Tipo->obtenerTodos();
		$this->load->view('header');
		$this->load->view('tipos/index',$data);
		$this->load->view('footer');
	}

    public function nuevo()
	{

		$this->load->view('header');
		$this->load->view('tipos/nuevo');
		$this->load->view('footer');
	}

    public function editar($id_te)
	{

        $data["tipoEditar"] = $this->Tipo->obtenerPorId($id_te);
		$this->load->view('header');
		$this->load->view('tipos/editar',$data);
		$this->load->view('footer');
	}

	public function guardar(){
		$datos = array(
			"nombre_te"=>$this->input->post("nombre_te"),
				 "estado_te"=>$this->input->post("estado_te"),
			"creacion_te"=>$this->input->post("creacion_te"),
				 "actualizacion_te"=>$this->input->post("actualizacion_te"),


		);
		//validamos los campos del formulario
		if ($this->Tipo->insertar($datos))
		{
			//llamamos la libraria session user_data solo para una vez y flash_data para varias

		}else {

		}
		redirect ('tipos/index');

	}

	public function eliminar($id_te){

		  if ($this->Tipo->eliminar($id_te)) {

		  } else {


		  }redirect ('tipos/index');
	}

    public function procesoActualizar(){
        $datos = array(
          "nombre_te"=>$this->input->post("nombre_te"),
    				 "estado_te"=>$this->input->post("estado_te"),
    			"creacion_te"=>$this->input->post("creacion_te"),
    				 "actualizacion_te"=>$this->input->post("actualizacion_te"),

		);
        $id_te=$this->input->post("id_te");
		//validamos los campos del formulario
		if ($this->Tipo->actualizar($id_te,$datos))
		{
			//llamamos la libraria session user_data solo para una vez y flash_data para varias

		}else {

		}
		redirect ('tipos/index');

    }
}
