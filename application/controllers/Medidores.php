<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Medidores extends CI_Controller {

	public function __construct() {
		parent::__construct();

        $this->load->model("Medidor");
		

	}

	public function index()
	{
		$data["medidor"]=$this->Medidor->obtenerTodos();
		$this->load->view('header');
		$this->load->view('medidores/index',$data);
		$this->load->view('footer');
	}

    public function nuevo()
	{

		$this->load->view('header');
		$this->load->view('medidores/nuevo');
		$this->load->view('footer');
	}

    public function editar($id_med)
	{

        $data["medidorEditar"] = $this->Medidor->obtenerPorId($id_med);
		$this->load->view('header');
		$this->load->view('medidores/editar',$data);
		$this->load->view('footer');
	}

	public function guardar(){
		$datos = array(
            "fk_id_rut"=>$this->input->post("fk_id_rut"),
			"fk_id_tar"=>$this->input->post("fk_id_tar"),
            "numero_med"=>$this->input->post("numero_med"),
			"serie_med"=>$this->input->post("serie_med"),
            "marca_med"=>$this->input->post("marca_med"),
            "observacion_med"=>$this->input->post("observacion_med"),
            "estado_med"=>$this->input->post("estado_med"),
            "foto_med"=>$this->input->post("foto_med"),
            "creacion_med"=>$this->input->post("creacion_med"),
            "actualizacion_med"=>$this->input->post("actualizacion_med"),
            "lectura_inicial_med"=>$this->input->post("lectura_inicial_med"),


		);
		//validamos los campos del formulario
		if ($this->Medidor->insertar($datos))
		{
			//llamamos la libraria session user_data solo para una vez y flash_data para varias

		}else {

		}
		redirect ('medidores/index');

	}

	public function eliminar($id_med){

		  if ($this->Medidor->eliminar($id_med)) {

		  } else {


		  }redirect ('medidores/index');
	}

    public function procesoActualizar(){
        $datos = array(
          "fk_id_rut"=>$this->input->post("fk_id_rut"),
    "fk_id_tar"=>$this->input->post("fk_id_tar"),
          "numero_med"=>$this->input->post("numero_med"),
    "serie_med"=>$this->input->post("serie_med"),
          "marca_med"=>$this->input->post("marca_med"),
          "observacion_med"=>$this->input->post("observacion_med"),
          "estado_med"=>$this->input->post("estado_med"),
          "foto_med"=>$this->input->post("foto_med"),
          "creacion_med"=>$this->input->post("creacion_med"),
          "actualizacion_med"=>$this->input->post("actualizacion_med"),
          "lectura_inicial_med"=>$this->input->post("lectura_inicial_med"),

		);
        $id_med=$this->input->post("id_med");
		//validamos los campos del formulario
		if ($this->Medidor->actualizar($id_med,$datos))
		{
			//llamamos la libraria session user_data solo para una vez y flash_data para varias

		}else {

		}
		redirect ('medidores/index');

    }
}
