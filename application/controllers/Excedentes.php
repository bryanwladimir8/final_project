<?php 
class Excedentes extends CI_Controller{

    function __construct(){
        parent::__construct();
        $this->load->model('Excedente');
    }
    //funciones que reenderiza a vista index
    public function index(){
        $datos['excedentesIndex']=$this->Excedente->ObtenerExcedentes();
        $this->load->view('header');
        $this->load->view('/excedentes/index',$datos);
        $this->load->view('footer');
    }
    //funciones que reenderiza a vista nuevo
    public function nuevo(){
        $this->load->view('header');
        $this->load->view('/excedentes/nuevo');
        $this->load->view('footer');
    }
    public function editar($id_ex){
        $datos['excedenteEditar']=$this->Excedente->obtenerExcedente($id_ex);
        $this->load->view('header');
        $this->load->view('/excedentes/editar',$datos);
        $this->load->view('footer');
    }
    public function GuardarExcedente(){
        $datosExcedente=array(
            "id_tar"=>$this->input->post('id_tar'),
			"limite_minimo_ex"=>$this->input->post('limite_minimo_ex'),
			"limite_maximo_ex"=>$this->input->post('limite_maximo_ex'),
      		"tarifa_ex"=>$this->input->post('tarifa_ex'),
			"fecha_actualizacion_ex"=>$this->input->post('fecha_actualizacion_ex'),
      		"fecha_creacion_ex"=>$this->input->post('fecha_creacion_ex'),
		);
        if ($this->Excedente->InsertarExcedente($datosExcedente)) {

        } else {

        }
        redirect("/Excedentes/index");
        
    }
    public function EditarExcedente(){
        $nuevosDatos=array(
            "id_tar"=>$this->input->post('id_tar'),
			"limite_minimo_ex"=>$this->input->post('limite_minimo_ex'),
			"limite_maximo_ex"=>$this->input->post('limite_maximo_ex'),
      		"tarifa_ex"=>$this->input->post('tarifa_ex'),
			"fecha_actualizacion_ex"=>$this->input->post('fecha_actualizacion_ex'),
      		"fecha_creacion_ex"=>$this->input->post('fecha_creacion_ex'),

        );
        // print_r($nuevosDatos);
        $id_ex = $this->input->post('id_ex');
        if ($this->Excedente->actualizarExcedente($id_ex,$nuevosDatos)) {
            redirect("/Excedentes/index");
        } else {
            echo "No se pudo actualizar";
        }
        

    }
        //funcion para eliminar 

    public function Eliminar($id_ex){
        if ($this->Excedente->borrarExcedente($id_ex)) {

        } else {

        }
        redirect("/Excedentes/index");
        
}  
}





?>