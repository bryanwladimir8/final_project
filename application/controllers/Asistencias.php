<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Asistencias extends CI_Controller {

	public function __construct() {
		parent::__construct();

        $this->load->model("Asistencia");

		$this->load->model("Socio");

	}

	public function index()
	{
		$data["asistencia"]=$this->Asistencia->obtenerTodos();
		$this->load->view('header');
		$this->load->view('asistencias/index',$data);
		$this->load->view('footer');
	}

    public function nuevo()
	{

		$this->load->view('header');
		$this->load->view('asistencias/nuevo');
		$this->load->view('footer');
	}

    public function editar($id_asi)
	{

        $data["asistenciaEditar"] = $this->Asistencia->obtenerPorId($id_asi);
		$this->load->view('header');
		$this->load->view('asistencias/editar',$data);
		$this->load->view('footer');
	}

	public function guardar(){
		$datos = array(
            "fk_id_eve"=>$this->input->post("fk_id_eve"),
			"fk_id_soc"=>$this->input->post("fk_id_soc"),
            "tipo_asi"=>$this->input->post("tipo_asi"),
			"valor_asi"=>$this->input->post("valor_asi"),
            "atraso_asi"=>$this->input->post("atraso_asi"),
            "valor_atraso_asi"=>$this->input->post("valor_atraso_asi"),
            "creacion_asi"=>$this->input->post("creacion_asi"),
            "actualizacion_asi"=>$this->input->post("actualizacion_asi"),

		);
		//validamos los campos del formulario
		if ($this->Asistencia->insertar($datos))
		{
			//llamamos la libraria session user_data solo para una vez y flash_data para varias

		}else {

		}
		redirect ('asistencias/index');

	}

	public function eliminar($id_asi){

		  if ($this->Asistencia->eliminar($id_asi)) {

		  } else {


		  }redirect ('asistencias/index');
	}

    public function procesoActualizar(){
        $datos = array(
          "fk_id_eve"=>$this->input->post("fk_id_eve"),
    "fk_id_soc"=>$this->input->post("fk_id_soc"),
          "tipo_asi"=>$this->input->post("tipo_asi"),
    "valor_asi"=>$this->input->post("valor_asi"),
          "atraso_asi"=>$this->input->post("atraso_asi"),
          "valor_atraso_asi"=>$this->input->post("valor_atraso_asi"),
          "creacion_asi"=>$this->input->post("creacion_asi"),
          "actualizacion_asi"=>$this->input->post("actualizacion_asi"),

		);
        $id_asi=$this->input->post("id_asi");
		//validamos los campos del formulario
		if ($this->Asistencia->actualizar($id_asi,$datos))
		{
			//llamamos la libraria session user_data solo para una vez y flash_data para varias

		}else {

		}
		redirect ('asistencias/index');

    }
}
