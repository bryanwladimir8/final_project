<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Impuestos extends CI_Controller {

	public function __construct() {
		parent::__construct();

        $this->load->model("Impuesto");

	}

	public function index()
	{
		$data["impuesto"]=$this->Impuesto->obtenerTodos();
		$this->load->view('header');
		$this->load->view('impuestos/index',$data);
		$this->load->view('footer');
	}

    public function nuevo()
	{

		$this->load->view('header');
		$this->load->view('impuestos/nuevo');
		$this->load->view('footer');
	}

    public function editar($id_imp)
	{

        $data["impuestoEditar"] = $this->Impuesto->obtenerPorId($id_imp);
		$this->load->view('header');
		$this->load->view('impuestos/editar',$data);
		$this->load->view('footer');
	}

	public function guardar(){
		$datos = array(
			"nombre_imp"=>$this->input->post("nombre_imp"),
		  "descripcion_imp"=>$this->input->post("descripcion_imp"),
			"porcentaje_imp"=>$this->input->post("porcentaje_imp"),
			"retencion_imp"=>$this->input->post("retencion_imp"),
			"estado_imp"=>$this->input->post("estado_imp"),
			"creacion_con"=>$this->input->post("creacion_con"),
			"creacion_imp"=>$this->input->post("creacion_imp"),
			"actualizacion_imp"=>$this->input->post("actualizacion_imp"),
		);
		//validamos los campos del formulario
		if ($this->Impuesto->insertar($datos))
		{
			//llamamos la libraria session user_data solo para una vez y flash_data para varias

		}else {

		}
		redirect ('impuestos/index');

	}

	public function eliminar($id_imp){

		  if ($this->Impuesto->eliminar($id_imp)) {

		  } else {


		  }redirect ('impuestos/index');
	}

    public function procesoActualizar(){
        $datos = array(
          "nombre_imp"=>$this->input->post("nombre_imp"),
    		  "descripcion_imp"=>$this->input->post("descripcion_imp"),
    			"porcentaje_imp"=>$this->input->post("porcentaje_imp"),
    			"retencion_imp"=>$this->input->post("retencion_imp"),
    			"estado_imp"=>$this->input->post("estado_imp"),
    			"creacion_con"=>$this->input->post("creacion_con"),
    			"creacion_imp"=>$this->input->post("creacion_imp"),
    			"actualizacion_imp"=>$this->input->post("actualizacion_imp"),

		);
        $id_imp=$this->input->post("id_imp");
		//validamos los campos del formulario
		if ($this->Impuesto->actualizar($id_imp,$datos))
		{
			//llamamos la libraria session user_data solo para una vez y flash_data para varias

		}else {

		}
		redirect ('impuestos/index');

    }
}
