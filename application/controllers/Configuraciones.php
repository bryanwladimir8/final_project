<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Configuraciones extends CI_Controller {

	public function __construct() {
		parent::__construct();

        $this->load->model("Configuracion");

	}

	public function index()
	{
		$data["configuracion"]=$this->Configuracion->obtenerTodos();
		$this->load->view('header');
		$this->load->view('configuraciones/index',$data);
		$this->load->view('footer');
	}

    public function nuevo()
	{

		$this->load->view('header');
		$this->load->view('configuraciones/nuevo');
		$this->load->view('footer');
	}

    public function editar($id_con)
	{

        $data["configuracionEditar"] = $this->Configuracion->obtenerPorId($id_con);
		$this->load->view('header');
		$this->load->view('configuraciones/editar',$data);
		$this->load->view('footer');
	}

	public function guardar(){
		$datos = array(
			"nombre_con"=>$this->input->post("nombre_con"),
				 "ruc_con"=>$this->input->post("ruc_con"),
			"telefono_con"=>$this->input->post("telefono_con"),
				 "direccion_con"=>$this->input->post("direccion_con"),
			"email_con"=>$this->input->post("email_con"),
			"servidor_con"=>$this->input->post("servidor_con"),
			"puerto_con"=>$this->input->post("puerto_con"),
			"password_con"=>$this->input->post("password_con"),
			"creacion_con"=>$this->input->post("creacion_con"),
			"actualizacion_con"=>$this->input->post("actualizacion_con"),
			"anio_inicial_con"=>$this->input->post("anio_inicial_con"),
			"mes_inicial_con"=>$this->input->post("mes_inicial_con"),
		);
		//validamos los campos del formulario
		if ($this->Configuracion->insertar($datos))
		{
			//llamamos la libraria session user_data solo para una vez y flash_data para varias

		}else {

		}
		redirect ('configuraciones/index');

	}

	public function eliminar($id_con){

		  if ($this->Configuracion->eliminar($id_con)) {

		  } else {


		  }redirect ('configuraciones/index');
	}

    public function procesoActualizar(){
        $datos = array(
					"nombre_con"=>$this->input->post("nombre_con"),
						 "ruc_con"=>$this->input->post("ruc_con"),
					"telefono_con"=>$this->input->post("telefono_con"),
						 "direccion_con"=>$this->input->post("direccion_con"),
					"email_con"=>$this->input->post("email_con"),
					"servidor_con"=>$this->input->post("servidor_con"),
					"puerto_con"=>$this->input->post("puerto_con"),
					"password_con"=>$this->input->post("password_con"),
					"creacion_con"=>$this->input->post("creacion_con"),
					"actualizacion_con"=>$this->input->post("actualizacion_con"),
					"anio_inicial_con"=>$this->input->post("anio_inicial_con"),
					"mes_inicial_con"=>$this->input->post("mes_inicial_con"),

		);
        $id_con=$this->input->post("id_con");
		//validamos los campos del formulario
		if ($this->Configuracion->actualizar($id_con,$datos))
		{
			//llamamos la libraria session user_data solo para una vez y flash_data para varias

		}else {

		}
		redirect ('configuraciones/index');

    }
}
